import Vue from 'vue'
import Router from 'vue-router'
import plugin from '@/views/plugin'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/plugin',
      name: 'plugin',
      component: plugin
    }
  ]
})
