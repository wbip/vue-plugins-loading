const webpack = require('webpack');
const path = require('path');
const utils = require('./utils');
const config = require('../config')
const merge = require('webpack-merge')

//const VueLoaderPlugin = require('vue-loader/lib/plugin')
 const { VueLoaderPlugin } = require('vue-loader')
const vueLoaderConfig = require('./vue-loader.conf')

const {entry, mkdirsSync} = require('./output-plugin')

function resolve(dir) {
  return path.join(__dirname, '..', dir)
}

mkdirsSync(resolve('/static/plugins'))

var baseWebpackConfig = {
  mode: "production", //development、production
  entry: entry,
  output: {
    path: resolve('/static/plugins'),
    filename: '[name].js',
  },
  resolve: {
    extensions: ['.js', '.vue', '.json', '.ts', '.jsx', '.tsx'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@': resolve('src'),
    }
  },
  externals: {
    vue: 'vue',
    axios: 'axios'
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options:vueLoaderConfig
        // options: {
        //     compilerOptions: {
        //         preserveWhitespace: false
        //     }
        // }
      },
      {
        test: /\.tsx?$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'babel-loader',
					},
				],
			},
      {
        test: /\.m?jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: utils.assetsPath('img/[name].[hash:7].[ext]')
        }
      },
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: utils.assetsPath('media/[name].[hash:7].[ext]')
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: utils.assetsPath('fonts/[name].[ext]')
        }
      }
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new webpack.optimize.ModuleConcatenationPlugin() // 提升模块作用域，降低文件打包体积
  ]
};

const webpackConfig = merge(baseWebpackConfig, {
  module: {
    noParse: /jquery|loadash/, //忽略未采用模块化的文件，因此jQuery或loadash将不会被下面的loaders解析
    rules: utils.styleLoaders({
      sourceMap: config.build.productionSourceMap,
      extract: false,
      usePostCSS: true
    })
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': '"production"'
    }),
    // new webpack.DefinePlugin({
    //   'process.env.NODE_ENV': require('../config/dev.env')
    // }),
    
  ]
})

module.exports = webpackConfig