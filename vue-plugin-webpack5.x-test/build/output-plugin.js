const glob = require('glob')
const fs = require('fs');
const path = require('path');
// 遍历要打包的组件
let entry = {}
var moduleSrcArray = glob.sync('./packages/plugins/*')
for(var x in moduleSrcArray){
  let fileName = (moduleSrcArray[x].split('/')[3]).slice(0, -4)
  entry[fileName] = moduleSrcArray[x]
}

// 清理文件
function mkdirsSync(dirname) {
  if (fs.existsSync(dirname)) {
    deleteall(dirname)
    return true;
  } else {
    if (mkdirsSync(path.dirname(dirname))) {
      fs.mkdirSync(dirname);
      return true;
    }
  }
}
// 删除文件下的文件
function deleteall(path) {
  var files = [];
  if(fs.existsSync(path)) {
    files = fs.readdirSync(path);
    files.forEach(function(file, index) {
      var curPath = path + "/" + file;
      if(fs.statSync(curPath).isDirectory()) { // recurse
        deleteall(curPath);
      } else { // delete file
        fs.unlinkSync(curPath);
      }
    });
  }
};

exports.entry = entry
exports.mkdirsSync = mkdirsSync