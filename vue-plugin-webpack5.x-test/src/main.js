import Vue from 'vue';
import App from './App'
import router from './router'
import axios from 'axios'


Vue.prototype.$axios = axios
window.Vue = Vue

//test jsx support
// const testJsx = Vue.extend({
//     render(h){
//         return <div>wuhaihao</div>
//     }
// })
// import CompTest from './components/component-test'


// new Vue({
//     el:'#app',
//     components:{CompTest},
//     render(h){
//         return h('div',[
//             h(testJsx),
//             h(CompTest)
//         ]);
//     }
// })


new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
})
