import Vue from 'vue'
import Router from 'vue-router'
import loadAsynPlugin from '@/views/loadAsynPlugin'
// import newFunction from '@/views/newFunction'
import unBuildPlugin from '@/views/unBuildPlugin'
import vueAsync from '@/views/vueAsync'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/loadAsynPlugin',
      name: 'loadAsynPlugin',
      component: loadAsynPlugin
    },
    // {
    //   path: '/newFunction',
    //   name: 'newFunction',
    //   component: newFunction
    // },
    // {
    //   path: '/unBuildPlugin',
    //   name: 'unBuildPlugin',
    //   component: unBuildPlugin
    // },
    {
      path: '/vueAsync',
      name: 'vueAsync',
      component: vueAsync
    }

  ]
})
