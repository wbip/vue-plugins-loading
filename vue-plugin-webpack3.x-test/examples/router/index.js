import Vue from 'vue'
import Router from 'vue-router'
import plugin from '../views/plugin'
import umdPlugin from '../views/umdPlugin'
import vueAsync from '../views/vueAsync'
import testRemote from '../views/remote/testRemote'


Vue.use(Router)

export default new Router({
  routes: [
    { // 测试esmodule组件加载
      path: '/plugin',
      name: 'plugin',
      component: plugin
    },
    { // 测试umd模块组件加载
      path: '/umdPlugin',
      name: 'umdPlugin',
      component: umdPlugin
    },
    { // 测试vue的异步加载
      path: '/vueAsync',
      name: 'vueAsync',
      component: vueAsync
    },
    { // 测试vue的异步加载
      path: '/testRemote',
      name: 'testRemote',
      component: testRemote
    },
  ]
})
