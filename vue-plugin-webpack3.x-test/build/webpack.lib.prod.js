var path = require('path')
var webpack = require('webpack')
var config = require('../config')
var utils = require('./utils')
var vueLoaderConfig = require('./vue-loader.conf')
var baseWebpackConfig = require('./webpack.base.conf')

var merge = require('webpack-merge')
var ExtractTextPlugin = require('extract-text-webpack-plugin')

function resolve (dir) {
  return path.join(__dirname, '..', dir)
}

// 整理入口
const components = require('../components.json')
const entrys = {}
Object.keys(components).forEach(item => {
  entrys[item] = components[item]
})

var webpackConfig = merge(baseWebpackConfig, {
  entry: entrys,
  module: {
    rules: utils.styleLoaders({
      sourceMap: config.build.productionSourceMap,
      usePostCSS: true,
      extract: true
    })
  },
  output: {
    path: path.resolve(config.build.assetsRoot, 'lib'),
    publicPath: '/',
    filename: "[name].js",
    chunkFilename: "[name].js",
    library: 'plugin-test',
    libraryTarget: 'umd',
    umdNamedDefine: true,
    pathinfo: true //给文件添加打包信息
  },
  externals: {
    vue: {
      root: 'Vue',
      commonjs: 'vue',
      commonjs2: 'vue',
      amd: 'vue'
    }
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],//此处设置要特别注意
    alias: {
      'vue$': 'vue/dist/vue.common.js',
      'src': resolve('src'),
      'utils': resolve("src/utils")
    }
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: vueLoaderConfig
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: utils.assetsPath('img/[name].[hash:7].[ext]')
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: utils.assetsFontPath('fonts/[name].[hash:7].[ext]')
        }
      }
    ]
  },
  plugins: [
      new webpack.DefinePlugin({
        'process.env': config.build.env
      }),
      //提升模块作用域,降低文件打包体积
      new webpack.optimize.ModuleConcatenationPlugin(),
      new webpack.optimize.UglifyJsPlugin({compress: {warnings: false},output: {comments: false}}),
      new ExtractTextPlugin({
        filename: '/theme/[name].css'
      })
  ]
})


// 分析打包文件大小
if (config.build.bundleAnalyzerReport) {
  const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
  webpackConfig.plugins.push(new BundleAnalyzerPlugin())
}

module.exports = webpackConfig