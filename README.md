# vue-plugins

## 1.直接在dom下挂载

```shell
export default {
  mounted() {
    const s = document.createElement('script');
    s.type = 'text/javascript';
    s.src = 'https://g.alicdn.com/dingding/dinglogin/0.0.2/ddLogin.js';
    document.body.appendChild(s);
  },
}
```

## 2.使用 createElement 方法，// 使用 <dingtalk></dingtalk> 在页面中调用

```shell
export default {
  components: {
    'dingtalk': {
      render(createElement) {
        return createElement(
          'script',
          {
            attrs: {
              type: 'text/javascript',
              src: 'https://g.alicdn.com/dingding/dinglogin/0.0.2/ddLogin.js',
            },
          },
        );
      },
    },
  },
}
```

3.通过封装一个组件 remote-js 实现

```shell
export default {
  components: {
   'remote-js': {
    render(createElement) {
      return createElement('script', { attrs: { type: 'text/javascript', src: this.src }});
    },
    props: {
      src: { type: String, required: true },
    },
  },
  },
}
```

<remote-js src="https://g.alicdn.com/dingding/dinglogin/0.0.2/ddLogin.js"></remote-js>