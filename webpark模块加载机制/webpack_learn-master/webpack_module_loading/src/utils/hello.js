// 关联模块：./util
import { util } from './util';

console.log('hello 依赖 util:', util);

const hello = 'Hello';

export default function sayHello() {
  console.log('the output is:');
  return hello;
};

export function cube(a, b) {
  console.log('the res is:');
  return a + b;
}